from django import forms

class LoginForm(forms.Form):
    username_attrs = {
        'class' : 'form-control',
        'placeholder' : 'Username',
        'type' : 'text',
        'required' : True
    }
    password_attrs = {
        'class' : 'form-control',
        'placeholder' : 'Password',
        'type' : 'password',
        'required' : True
    }

    username = forms.CharField(label="Username", max_length=30, widget=forms.TextInput(attrs=username_attrs))
    password = forms.CharField(label="Password", max_length=30, widget=forms.TextInput(attrs=password_attrs))

class RegisterForm(forms.Form):
    name_attrs = {
        'class' : 'form-control',
        'placeholder' : 'Name',
        'type' : 'text',
        'required' : True
    }
    username_attrs = {
        'class' : 'form-control',
        'placeholder' : 'letters, digits, and @/./+/-/_ only',
        'type' : 'text',
        'required' : True
    }
    password_attrs = {
        'class' : 'form-control',
        'placeholder' : 'contain minimal 8 characters, can’t be all numeric',
        'type' : 'password',
        'required' : True
    }
    email_attrs = {
        'class' : 'form-control',
        'placeholder' : 'example@mail.com',
        'type' : 'text',
        'required' : True
    }

    # name = forms.CharField(label="Name", max_length=100, widget=forms.TextInput(attrs=name_attrs))
    username = forms.CharField(label="Username", max_length=30, widget=forms.TextInput(attrs=username_attrs))
    password = forms.CharField(label="Password", max_length=30, widget=forms.TextInput(attrs=password_attrs))
    email = forms.EmailField(label="E-mail", max_length=50, widget=forms.EmailInput(attrs=email_attrs))
