from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from .forms import LoginForm, RegisterForm
from django.contrib import messages

# Create your views here.
def log_in(request):
	isvalid = True
	if request.method == "POST":
		form = LoginForm(request.POST)
		if form.is_valid():
			user_name = form.cleaned_data["username"]
			pass_word = form.cleaned_data["password"]
			user = authenticate(request, username = user_name, password = pass_word)
			if user is not None:
				login(request, user)
				return redirect("/")
			else:
				messages.error(request, "Invalid username or password, please try again.")			
				isvalid = False
	form = LoginForm()
	response = {
		"form" : form,
		"isvalid" : isvalid
	}
	return render(request, "login.html", response)

def log_out(request):
    logout(request)
    return redirect("/")

def register(request):
	isvalid = True
	if request.method == "POST":
		form = RegisterForm(request.POST)
		if (form.is_valid()):
			user_name = form.cleaned_data["username"]
			e_mail = form.cleaned_data["email"]
			pass_word = form.cleaned_data["password"]
			try:
				user = User.objects.get(username = user_name)
				messages.error(request, "Username already exist, please choose another username.")			
				isvalid = False		
				return redirect("/register/")
			except User.DoesNotExist:
				user = User.objects.create_user(username=user_name, email=e_mail, password=pass_word)
				isvalid = True;
				messages.success(request, "Sign up successful! Please log in.")
				return redirect("/login/")
		else:
			messages.error(request, "Invalid components. Please re-check your e-mail and password and try again.")			
			isvalid = False

	form = RegisterForm()
	response = {
		"form" : form,
		"isvalid" : isvalid
	}
	return render(request, "signup.html", response)