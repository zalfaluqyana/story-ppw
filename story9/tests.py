from django.test import TestCase, Client
from django.urls import resolve
from django.apps import apps
from .apps import Story9Config
from .views import log_in, log_out, register
from django.http import HttpRequest
from django.contrib.messages import get_messages

# Create your tests here.

class TestStory9(TestCase):
	def test_if_login_url_exist(self):
		response = Client().get('/login/')
		self.assertEquals(response.status_code, 200)

	def test_if_logout_url_exist(self):
		response = Client().get('/logout/')
		self.assertEquals(response.status_code, 302)

	def test_if_register_url_exist(self):
		response = Client().get('/register/')
		self.assertEquals(response.status_code, 200)

	def test_if_login_url_uses_login_template(self):
		response = Client().get('/login/')
		self.assertTemplateUsed(response, 'login.html')

	def test_if_register_url_uses_register_template(self):
		response = Client().get('/register/')
		self.assertTemplateUsed(response, 'signup.html')

	def test_login_url_resolves_login_view(self):
		found = resolve('/login/')
		self.assertEquals(found.func, log_in)

	def test_logout_url_resolves_logout_view(self):
		found = resolve('/logout/')
		self.assertEquals(found.func, log_out)

	def test_register_url_resolves_register_view(self):
		found = resolve('/register/')
		self.assertEquals(found.func, register)

	def test_if_login_test_appear_inside_html(self):
		request = HttpRequest()
		response = log_in(request)
		html_response = response.content.decode('utf8')
		self.assertIn("Log in", html_response)

	def test_register_test(self):
		response = self.client.post('/register/', data={'username' : 'zalfalqyn', 'email':'zalfalqyn@yahoo.com', 'password' : 'zalfaluq123'})
		self.assertEqual(response.status_code, 302)

	def test_register_already_exist_test(self):
		response1 = self.client.post('/register/', data={'username' : 'zalfalqyn', 'email':'zalfalqyn@yahoo.com', 'password' : 'zalfaluq123'})
		response2 = self.client.post('/register/', data={'username' : 'zalfalqyn', 'email':'zalfalqyn@yahoo.com', 'password' : 'zalfaluq123'})
		messages = [m.message for m in get_messages(response2.wsgi_request)]
		self.assertIn('Username already exist, please choose another username.', messages)

	def test_if_error_alert_shows_when_signup_failed(self):
		response = Client().post('/register/', {'username' : 'a', 'email':'a', 'password' : 'a'})
		messages = [m.message for m in get_messages(response.wsgi_request)]
		self.assertIn('Invalid components. Please re-check your e-mail and password and try again.', messages)

	def test_app_name(self):
		self.assertEquals(Story9Config.name, 'story9')
		self.assertEquals(apps.get_app_config('story9').name, 'story9')