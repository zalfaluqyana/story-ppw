$(document).ready(function() {
	$("#keyword").on("keyup", function(e) {
		var to_find = e.currentTarget.value;
		console.log(to_find);

		$.ajax({
			url: '/data?q=' + to_find,
			success: function(data) {
				var content = data.items;
				console.log(content);
				$("#booklist").empty();
				for (i=0; i<content.length; i++) {
					var title = content[i].volumeInfo.title;
					var author = content[i].volumeInfo.authors;
					var pic = content[i].volumeInfo.imageLinks.smallThumbnail;
					var category = content[i].volumeInfo.categories;
					var original_desc = content[i].volumeInfo.description;
					var url = content[i].volumeInfo.infoLink;

					if (original_desc) {
						var desc = content[i].volumeInfo.description.substring(0, 200);
						if (original_desc.length > 200) {
							desc+="...";
						}
					}
					$("#booklist").append(
						"<div class='col-md-2'><a target='_blank'href=" + url + "><img src="
						+ pic + "></div> <div class='col-md-4'><p class='title'><b>"
						+ title + "</b></p></a> <p>"+ author + "</b></p> <p>" + desc + "</p></div>");
				}
			}
		});
	})
})