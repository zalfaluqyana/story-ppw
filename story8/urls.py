from django.contrib import admin
from django.urls import path, include
from . import views

urlpatterns = [
    path('find_book/', views.find_book, name='find_book'),
    path('data/', views.book_list, name='book_list'),
]