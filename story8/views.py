from django.shortcuts import render
from django.http import JsonResponse
import requests
import json

# Create your views here.

def find_book(request):
	response = {}
	return render(request, 'story8.html', response)

def book_list(request):
	q = request.GET['q']
	url = "https://www.googleapis.com/books/v1/volumes?q=" + q
	result = requests.get(url)
	data = json.loads(result.content)
	return JsonResponse(data, safe=False)