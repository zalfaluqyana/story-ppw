from django.test import TestCase, Client
from django.urls import resolve
from django.apps import apps
from .apps import Story7Config

from .views import about_me
# Create your tests here.
class TestStory7(TestCase):
	def test_if_about_me_url_exist(self):
		response = Client().get('/about_me/')
		self.assertEquals(response.status_code, 200)

	def test_if_about_me_url_uses_story7_template(self):
		response = Client().get('/about_me/')
		self.assertTemplateUsed(response, 'story7.html')

	def test_app_name(self):
		self.assertEquals(Story7Config.name, 'story7')
		self.assertEquals(apps.get_app_config('story7').name, 'story7')

	def test_about_me_url_resolves_to_about_me_view(self):
		found = resolve('/about_me/')
		self.assertEquals(found.func, about_me)