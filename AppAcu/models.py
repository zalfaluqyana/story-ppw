from django.db import models

# Create your models here.
class Schedule(models.Model):
	matkul = models.CharField(blank=False, max_length=100)
	dosen = models.CharField(blank=False, max_length=100)
	desc = models.TextField(blank=False)
	sks = models.PositiveIntegerField()
	semester = models.CharField(blank=False, max_length=50)
	ruang = models.CharField(blank=False, max_length=10)