from django.shortcuts import render
from django.shortcuts import redirect
from .forms import FormSchedule
from .models import Schedule

def home(request):
    name = 'zalfa'
    return render(request, 'home.html', {'name':name})

def content(request):
    return render(request, 'content.html')

def gallery(request):
    return render(request, 'gallery.html')

def formSchedule(request):
	if request.method=='POST':
		form = FormSchedule(request.POST)
		if form.is_valid():
			data = Schedule()
			data.matkul = form.cleaned_data['matkul']
			data.dosen = form.cleaned_data['dosen']
			data.desc = form.cleaned_data['desc']
			data.sks = form.cleaned_data['sks']
			data.semester = form.cleaned_data['semester']
			data.ruang = form.cleaned_data['ruang']
			data.save()
			return redirect('/schedule/isi')
		return render(request, 'schedule.html', {'form' : form})
	form = FormSchedule()
	return render(request, 'schedule.html', {'form' : form})

def isischedule(request):
	schedule = Schedule.objects.all()
	response = {
	'schedule':schedule
	}
	return render(request, 'isi-schedule.html', response)

def deleteSchedule(request, id):
	Schedule.objects.filter(id=id).delete()
	schedule = Schedule.objects.all()
	response = {
	'schedule':schedule
	}
	return render(request, 'isi-schedule.html', response)

def detailSchedule(request, id):
	schedule = Schedule.objects.get(id=id)
	response = {
	'schedule':schedule
	}
	return render(request, 'detail-schedule.html', response)