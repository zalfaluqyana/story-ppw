"""Project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from . import views
# bisa from .views import nama function

urlpatterns = [
    path('', views.content, name='content'),
    path('story1/', views.home, name='home'),
    path('gallery/', views.gallery, name='gallery'),
    path('schedule/', views.formSchedule),
    path('schedule/isi/', views.isischedule),
    path('schedule/isi/<int:id>', views.deleteSchedule, name='deleteSchedule'),
    path('schedule/detail/<int:id>', views.detailSchedule, name='detailSchedule')
]
