# Generated by Django 3.1.1 on 2020-10-15 15:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('AppAcu', '0003_auto_20201015_2231'),
    ]

    operations = [
        migrations.AlterField(
            model_name='schedule',
            name='sks',
            field=models.PositiveIntegerField(),
        ),
    ]
