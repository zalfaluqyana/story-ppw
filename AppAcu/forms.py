from django import forms
from .models import Schedule

class FormSchedule(forms.Form):

	SMT_YEAR = [
		('Gasal 2015/2016', 'Gasal 2015/2016'),
		('Gasal 2016/2017', 'Gasal 2016/2017'),
		('Genap 2016/2017', 'Genap 2016/2017'),
		('Gasal 2017/2018', 'Gasal 2017/2018'),
		('Genap 2017/2018', 'Genap 2017/2018'),
		('Gasal 2018/2019', 'Gasal 2018/2019'),
		('Genap 2018/2019', 'Genap 2018/2019'),
		('Gasal 2019/2020', 'Gasal 2019/2020'),
		('Genap 2019/2020', 'Genap 2019/2020'),
		('Gasal 2020/2021', 'Gasal 2020/2021')
	]

	matkul_attrs = {
		'class' : 'form-control',
		'type' : 'text',
		'required' : True,
		'placeholder' : 'Mata Kuliah'
	}

	dosen_attrs = {
		'class' : 'form-control',
		'required' : True,
		'placeholder' : 'Dosen Pengajar'
	}

	desc_attrs = {
		'class' : 'form-control',
		'type' : 'text',
		'required' : True,
		'placeholder' : 'Deskripsi'
	}

	sks_attrs = {
		'class' : 'form-control',
		'type' : 'number',
		'required' : True,
		'placeholder' : 'Jumlah SKS: 1, 2, etc.'
	}

	semester_attrs = {
		'class' : 'form-control',
		'type' : 'text',
		'required' : True
	}

	ruang_attrs = {
		'class' : 'form-control',
		'type' : 'text',
		'required' : True,
		'placeholder' :'Ruang Kelas'
	}




	matkul = forms.CharField(label = "Mata Kuliah", max_length=100, widget=forms.TextInput(attrs=matkul_attrs))
	dosen = forms.CharField(label = 'Dosen Pengajar', max_length=100, widget=forms.TextInput(attrs=dosen_attrs))
	desc = forms.CharField(label = 'Deskripsi Mata Kuliah', widget=forms.TextInput(attrs=desc_attrs))
	sks = forms.IntegerField(label='Jumlah SKS', min_value=1, max_value=10, widget=forms.NumberInput(attrs=sks_attrs))
	semester = forms.ChoiceField(label = 'Semester/Tahun', choices=SMT_YEAR, widget=forms.Select(attrs=semester_attrs))
	ruang = forms.CharField(label = 'Ruang Kelas', max_length=10, widget=forms.TextInput(attrs=ruang_attrs))