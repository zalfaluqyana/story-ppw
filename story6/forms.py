from django import forms
from .models import Activity, Registrant

class FormActivity(forms.Form):

	activity_name_attrs = {
		'class' : 'form-control',
		'type' : 'text',
		'required' : True,
		'placeholder' : 'ex: Swimming'
	}

	activity_desc_attrs = {
		'class' : 'form-control',
		'type' : 'text',
		'required' : True,
		'placeholder' : 'Description of the activity'
	}


	activity = forms.CharField(label = "Name of Activity", max_length=100, widget=forms.TextInput(attrs=activity_name_attrs))
	desc_activity = forms.CharField(label = 'Activity Description', widget=forms.TextInput(attrs=activity_desc_attrs))


class FormRegistrant(forms.Form):

	registrant_attrs = {
		'class' : 'form-control',
		'type' : 'text',
		'required' : True,
		'placeholder' : 'ex: Zalfa Luqyana'
	}

	name = forms.CharField(label = "Name", max_length=100, widget=forms.TextInput(attrs=registrant_attrs))