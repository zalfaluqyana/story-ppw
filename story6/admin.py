from django.contrib import admin

# Register your models here.
from .models import Activity, Registrant
admin.site.register(Activity)
admin.site.register(Registrant)