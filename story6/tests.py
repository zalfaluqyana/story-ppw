from django.test import TestCase, Client
from .models import Activity, Registrant
from django.apps import apps
from .apps import Story6Config

# Create your tests here.
class TestZalfa(TestCase):
	def test_if_activity_url_exist(self):
		response = Client().get('/activity/')
		self.assertEquals(response.status_code, 200)

	def test_if_activity_content_url_exist(self):
		response = Client().get('/activity/content/')
		self.assertEquals(response.status_code, 200)
	
	def test_if_registration_url_exist(self):
		aktivitas = Activity.objects.create(activity="sleep", desc_activity="sleeping")
		response = Client().get('/activity/registration/{0}/'.format(aktivitas.id))
		self.assertEquals(response.status_code, 200)

	def test_if_activity_url_uses_the_add_activity_template(self):
		response = Client().get('/activity/')
		self.assertTemplateUsed(response, 'add_activity.html')

	def test_if_activity_content_url_uses_activity_content_template(self):
		response = Client().get('/activity/content/')
		self.assertTemplateUsed(response, 'activity_content.html')

	def test_if_registration_url_uses_registration_template(self):
		aktivitas = Activity.objects.create(activity="sleep", desc_activity="sleeping")
		response = Client().get('/activity/registration/{0}/'.format(aktivitas.id))
		self.assertTemplateUsed(response, 'registration.html')

	def test_if_activity_model_exist(self):
		Activity.objects.create(activity="sleep", desc_activity="sleeping")
		num_of_activities = Activity.objects.all().count()
		self.assertEquals(num_of_activities, 1)

	def test_if_registrant_model_exist(self):
		aktivitas = Activity.objects.create(activity="sleep", desc_activity="sleeping")
		Registrant.objects.create(name="me", chosen_activity=aktivitas)
		num_of_registrant = Registrant.objects.all().count()
		self.assertEquals(num_of_registrant, 1)

	def test_if_Add_Activity_text_exist_in_activity_url(self):
		response = Client().get('/activity/')
		html_response = response.content.decode('utf8')
		self.assertIn("Add Activity", html_response)

	def test_if_Name_of_Activity_text_and_Activity_Description_text_exist_in_activity_url(self):
		response = Client().get('/activity/')
		html_response = response.content.decode('utf8')
		self.assertIn("Name of Activity", html_response)
		self.assertIn("Activity Description", html_response)

	def test_if_Registration_text_and_Register_text_exist_in_registration_url(self):
		response = Client().get('/activity/')
		html_response = response.content.decode('utf8')
		self.assertIn("Add Activity", html_response)

	def test_if_no_activity_has_been_added_yet_text_show_up_when_there_is_no_activity(self):
		response = Client().get('/activity/content/')
		html_response = response.content.decode('utf8')
		self.assertIn("no activity has been added, yet", html_response)

	def test_if_Register_text_show_up_when_there_is_at_least_1_activity(self):
		aktivitas = Activity.objects.create(activity="sleep", desc_activity="sleeping")
		response = Client().get('/activity/content/')
		html_response = response.content.decode('utf8')
		self.assertIn("Register", html_response)

	def test_if_activity_name_and_activity_description_text_show_up_when_there_is_at_least_1_activity(self):
		aktivitas = Activity.objects.create(activity="sleep", desc_activity="sleeping")
		name = aktivitas.activity
		desc = aktivitas.desc_activity
		response = Client().get('/activity/content/')
		html_response = response.content.decode('utf8')
		self.assertIn(name, html_response)
		self.assertIn(desc, html_response)

	def test_if_add_new_activity_text_show_up_in_activity_content_url(self):
		response = Client().get('/activity/content/')
		html_response = response.content.decode('utf8')
		self.assertIn("Add New Activity", html_response)

	def test_if_registrants_name_show_up_when_they_registered_to_an_activity(self):
		aktivitas = Activity.objects.create(activity="sleep", desc_activity="sleeping")
		pendaftar = Registrant.objects.create(name="me", chosen_activity=aktivitas)
		nama = pendaftar.name
		response = Client().get('/activity/content/')
		html_response = response.content.decode('utf8')
		self.assertIn(nama, html_response)

	def test_if_content_url_saved_the_data_from_activity_form(self):
		Client().post('/activity/', {'activity': 'sleep', 'desc_activity': 'sleeping'} )
		toGet = Client().get('/activity/content/')
		html_response = toGet.content.decode('utf8')
		self.assertIn("sleep", html_response)
		self.assertIn("sleeping", html_response)
		self.assertIn("Register", html_response)

	def test_if_content_url_saved_the_data_from_registration_form(self):
		aktivitas = Activity.objects.create(activity="sleep", desc_activity="sleeping")
		Client().post('/activity/registration/{0}/'.format(aktivitas.id), {'name': 'me', 'chosen_activity': aktivitas} )
		toGet = Client().get('/activity/content/')
		html_response = toGet.content.decode('utf8')
		self.assertIn("me", html_response)
		self.assertIn(aktivitas.activity, html_response)

	def test_app_name(self):
		self.assertEquals(Story6Config.name, 'story6')
		self.assertEquals(apps.get_app_config('story6').name, 'story6')