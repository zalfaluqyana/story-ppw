from django.db import models

# Create your models here.
class Activity(models.Model):
	activity = models.CharField(blank=False, max_length=100)
	desc_activity = models.TextField(blank=False)

class Registrant(models.Model):
	name = models.CharField(max_length=100)
	chosen_activity = models.ForeignKey(Activity, on_delete=models.CASCADE)
