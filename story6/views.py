from django.shortcuts import render
from django.shortcuts import redirect
from .forms import FormActivity, FormRegistrant
from .models import Activity, Registrant
# Create your views here.
def add_activity(request):
	if request.method=='POST':
		form = FormActivity(request.POST)
		if form.is_valid():
			data = Activity()
			data.activity = form.cleaned_data['activity']
			data.desc_activity = form.cleaned_data['desc_activity']
			data.save()
			return redirect('/activity/content')
	form = FormActivity()
	return render(request, 'add_activity.html', {'form' : form})

def show_activity(request):
	activity = Activity.objects.all()
	registrant = Registrant.objects.all()
	response = {
	'activity':activity,
	'registrant':registrant
	}
	return render(request, 'activity_content.html', response)

def registration(request, activity_id):
	if request.method=='POST':
		form = FormRegistrant(request.POST)
		if form.is_valid():
			data = Registrant()
			data.name = form.cleaned_data['name']
			data.chosen_activity = Activity.objects.get(id=activity_id)
			data.save()
			return redirect('/activity/content')
	form = FormRegistrant()
	return render(request, 'registration.html', {'form' : form})