from django.contrib import admin
from django.urls import path, include
from . import views

urlpatterns = [
    path('activity/', views.add_activity, name='activity'),
    path('activity/content/', views.show_activity, name='showactivity'),
    path('activity/registration/<int:activity_id>/', views.registration, name='registration'),
]
